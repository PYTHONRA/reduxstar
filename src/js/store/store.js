import { configureStore } from "@reduxjs/toolkit";
import counterReducer from "./counterSlice";
export const store = configureStore({
  reducer: {
    counter: counterReducer,
  }, //début du store pour tout le projet
});
