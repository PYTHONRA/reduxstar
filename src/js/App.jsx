import React, { useState } from 'react'
import logo from '../css/logo.svg'
import '../css/App.css'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './store/counterSlice'


function App() {
  //const [count, setCount] = useState(0)

  const count = useSelector(state => state.counter.value)
  const dispatch = useDispatch();
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>Hello Vite + React!</p>
        <p>
          {/* <button type="button" onClick={() => setCount((count) => count + 1)}>
            count is: {count}
          </button> */}
          <button onClick={() => dispatch(increment())}>
            increment
          </button>
          <button onClick={() => dispatch(decrement())}>
            decrement
          </button>
        </p>
        <p><strong>{count}</strong></p>
        <p>
          Edit <code>App.jsx</code> and save to test HMR updates.
        </p>
        <p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
          {' | '}
          <a
            className="App-link"
            href="https://vitejs.dev/guide/features.html"
            target="_blank"
            rel="noopener noreferrer"
          >
            Vite Docs
          </a>
        </p>
      </header>
    </div>
  )
}

export default App
